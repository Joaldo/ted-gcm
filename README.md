# TED GCM

## Visão do Produto


O <b>UCSal cursos</b> é uma ferramenta de gestão educacional para o ambiente web/mobile que tem como objetivo tornar as informações dos cursos da universidade mais transparentes, através da unificação de dados de diversos locais em uma única plataforma. 
<br />
O software tem como principais usuários, os alunos da instituição, os candidatos a alunos da universidade, os professores, a coordenação dos cursos e NDE. Que tenham principalmente desejo em manter e consultar as grades dos cursos, os planos de aulas e programa de disciplinas dos cursos. Esse esforço resultará em um ambiente onde será possível ter melhor gestão dos dados relacionados aos cursos, um controle de acesso melhor e mais eficaz e melhor validação dos artefatos.


## Diferencial

|                            | UCSal Cursos        | TOTVS             | 
|:------                     |:-------------:      |:-------:          | 
|Integração com o classroom  | :heavy_check_mark:  | :x:               |
|Acessivel aos candidatos    | :heavy_check_mark:  | :x:               | 
|Consulta há grade dos cursos| :heavy_check_mark:  | :heavy_check_mark:|
|Acesso ao plano de aulas    | :heavy_check_mark:  | :x:               |
